import React from 'react'

const Colors = (props) => {
    const colors = props.colors
    const deleteFunction = props.deleteColor
    const editFunction = props.editColor
    
    const colorList = colors.length ? (
        colors.map(color => {
            return (
                <div className="collection-item" key={color.id}>
                    <div className="item-text">
                        <div className="color-box" style={{backgroundColor:'#'+ color.hex }}></div>
                        <input className="list-color-input" type="text" style={{color:'#'+ color.hex }} defaultValue={ color.hex } onChange={ (event) => {editFunction(event, color.id)} } />
                        <i className="far fa-edit edit-btn"></i>
                    </div>
                    <i className="fas fa-trash delete-btn" onClick={ () => {deleteFunction(color.id)} }></i>
                </div>
            )
        })
    ) : (<p className="center">There are not yet colors! Please add some...</p>)
            
    return (
        <div className="color-list collection">
            { colorList }
        </div>
    );
}

export default Colors;