import React, { Component } from 'react'

class AddColor extends Component {

    state = {
        hex: ''
    }

    textChanged = (event) => {
        this.setState({
            hex: event.target.value
        })
    }

    formSubmitted = (event) => {
        event.preventDefault();
        this.props.addColor(this.state)
        this.setState({
            hex: ''
        })
    }

    render() {
        return (
            <div className="add-form">
                <form onSubmit={ this.formSubmitted }>
                    <label htmlFor="">Add new color: </label>
                    <input type="text" className="new-color-input" onChange={ this.textChanged } value={ this.state.hex }/>
                    <input type="submit" className="btn-small red" value="Add Color" />
                </form>
            </div>
        )
    }
}

export default AddColor