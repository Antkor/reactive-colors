import React, { Component } from 'react';
import Colors from './Colors';
import AddColor from './AddColor'
import './App.css';

class App extends Component {

  state = {
    colors: [
      { id: 1, hex: '01579b' },
      { id: 2, hex: 'ff6600' }
    ]
  }

  addColor = (color) => {
    color.id = Date.now() + "-" + (Math.random() * 1000);
    let newColors = [...this.state.colors, color];

    this.setState({
      colors: newColors
    })
  }

  editColor = (event, colorId) => {
    const indexToChange = this.state.colors.findIndex( color => {
      return color.id === colorId;
    })

    const newColor = {...this.state.colors[indexToChange]}
    newColor.hex = event.target.value;

    const newColors = [...this.state.colors];
    newColors[indexToChange] = newColor;

    this.setState({
      colors: newColors
    });
  }

  deleteColor = (id) => {
    const newColors = this.state.colors.filter(color => {
      return color.id !== id;
    })
    this.setState({
      colors: newColors
    });
  }

  render() {
    return (
      <div className="reactive-colors container">
        <h3 className="light-blue-text text-darken-4 center">Color List</h3>
        <Colors colors={ this.state.colors } deleteColor={ this.deleteColor } editColor={ this.editColor } />
        <AddColor addColor={ this.addColor } />
      </div>
    );
  }
}

export default App;
