## Reactive Colors

Reactive Colors is a simple React application that lets you add and edit hexadecimal colors to a list

## Usage

Simply clone the app, run npm-install and then run npm start to spin up a local development server and watch the app in action